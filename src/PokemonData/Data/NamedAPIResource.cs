﻿using System.Text.Json.Serialization;

namespace PokemonData
{    
    public class NamedAPIResource
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("url")]
        public string Url { get; set; }
    }
}