﻿
using System.Text.Json;


namespace PokemonData
{
    internal class DataClient
    {
        private static readonly HttpClient client = new HttpClient();
        public async Task <Data> GetPokemonDataByNameAsync(string pokemonName)
        {
            var stringTask = await client.GetStringAsync($"https://pokeapi.co/api/v2/pokemon/{pokemonName}");
            var data = JsonSerializer.Deserialize<Data>(stringTask);            
            return data;
        }

    }
}
