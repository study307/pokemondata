﻿
using System.Text.Json.Serialization;


namespace PokemonData
{    
    public class Data
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("types")]
        public TypePokemon[] Types { get; set; }

        [JsonPropertyName("height")]
        public int Height { get; set; }

        [JsonPropertyName("is_default")]
        public bool IsDefault { get; set; }    

    }
}
