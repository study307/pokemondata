﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonData
{
    internal interface IOutput
    {
        void Output(PokemonData.Business.PokemonData pokemonData, string pathFile);
    }
}
