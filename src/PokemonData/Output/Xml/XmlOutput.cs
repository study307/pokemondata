﻿using System.Xml.Serialization;

namespace PokemonData.Output.Xml
{
    internal class XmlOutput: IOutput
    {
        public void Output(PokemonData.Business.PokemonData pokemonData, string pathFile)
        {         
            using (StreamWriter file = new StreamWriter(Path.Combine(pathFile, "PokemonData.xml")))
            {
                PokemonDataXml pokemonDataXml = new PokemonDataXml(pokemonData);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(PokemonDataXml));
                xmlSerializer.Serialize(file, pokemonDataXml);
                file.Close();
            }                      
            
        }
    }
}
