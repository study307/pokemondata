﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonData.Output.Xml
{    
    public class PokemonDataXml
    {
        public string Name { get; set; }
        public int Id { get; set; }           
        public PokemonDataXml(PokemonData.Business.PokemonData pokemonData)
        {
            this.Name = pokemonData.Name;
            this.Id = pokemonData.Id;            
        }
        public PokemonDataXml()
        {

        }

    }
}
