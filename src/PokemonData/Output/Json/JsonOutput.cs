﻿using System.Text.Json;

namespace PokemonData.Output.Json
{
    internal class JsonOutput: IOutput
    {
        public void Output(PokemonData.Business.PokemonData pokemonData, string pathFile)
        {
            string fileName = pathFile +"PokemonData.json";
            string jsonString = JsonSerializer.Serialize(pokemonData);
            File.WriteAllText(fileName, jsonString);            
        }
    }
}
