﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonData.Output.Xml;
using PokemonData.Output.Binary;
using PokemonData.Output.Json;
using PokemonData.Output.Csv;


namespace PokemonData
{
    internal static class OutputFactory
    {       
        public static List<IOutput> CreateOutputs(IReadOnlyList<string> fileType)
        {            
            List<IOutput> output = new List<IOutput>();                

            for (int i = 0; i < fileType.Count; i++)
            {
                if (fileType[i].ToLower() == FileTypes.Xml)
                {
                    output.Add(new XmlOutput());
                }

                if (fileType[i].ToLower() == FileTypes.Json)
                {
                    output.Add(new JsonOutput());
                }

                if (fileType[i].ToLower() == FileTypes.Binary)
                {
                    output.Add(new BinaryOutput());
                }

                if (fileType[i].ToLower() == FileTypes.Csv)
                {
                    output.Add(new CsvOutput());
                }
            }
            return output;         
                        
        }
    }
}
