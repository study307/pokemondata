﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonData.Output.Csv
{    
    public class PokemonDataCsv
    {
        public string Name { get; set; }
        public int Id { get; set; }        
        public int Height { get; set; }
        public bool Default { get; set; }
        public string Description
        {
            get { return $"{Name} ({Id}): {Height}"; }            
        }
        public PokemonDataCsv(PokemonData.Business.PokemonData pokemonData)
        {
            this.Name = pokemonData.Name;
            this.Id = pokemonData.Id;            
            this.Height = pokemonData.Height;
            this.Default = pokemonData.Default;            
        }
        public PokemonDataCsv()
        {

        }

    }
}

