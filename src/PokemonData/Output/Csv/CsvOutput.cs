﻿using CsvHelper;
using System.Globalization;

namespace PokemonData.Output.Csv
{
    internal class CsvOutput: IOutput
    {
        public void Output(PokemonData.Business.PokemonData pokemonData, string pathFile)
        {               
            using (var writer = new StreamWriter(Path.Combine(pathFile, "PokemonData.csv")))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                PokemonDataCsv pokemonDataCsv = new PokemonDataCsv(pokemonData);
                csv.WriteHeader<PokemonDataCsv>();
                csv.NextRecord();
                csv.WriteRecord(pokemonDataCsv);
            }               

        }
        
    }
}
