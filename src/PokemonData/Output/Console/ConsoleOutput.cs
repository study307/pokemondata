﻿
using Spectre.Console;

namespace PokemonData.Output.Console
{
    internal class ConsoleOutput: IOutput
    {
        public void Output(PokemonData.Business.PokemonData pokemonData, string pathFile)
        {
            AnsiConsole.MarkupLine($"[green]name:[/] [hotpink]{pokemonData.Name}[/]");
            AnsiConsole.MarkupLine($"[green]id:[/] [hotpink]{pokemonData.Id}[/]");
            /*foreach(TypePokemon type in info.Types)
            {
                AnsiConsole.MarkupLine($"[green]type:[/] [hotpink]{type.Type.Name}[/]");
            }*/
            AnsiConsole.MarkupLine($"[green]type:[/] [hotpink]{pokemonData.Type}[/]");
            AnsiConsole.MarkupLine($"[green]height:[/] [hotpink]{pokemonData.Height}[/]");
            if (pokemonData.Default == true)
            {
                AnsiConsole.MarkupLine("[green]default:[/] [hotpink]Yes[/]");
            }
            else
            {
                AnsiConsole.MarkupLine("[green]default:[/] [hotpink]No[/]");
            }         
                       
        }
    }
}
