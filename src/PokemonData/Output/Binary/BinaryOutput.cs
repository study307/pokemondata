﻿using System.Runtime.Serialization.Formatters.Binary;


namespace PokemonData.Output.Binary
{
    internal class BinaryOutput: IOutput
    {
        public void Output(PokemonData.Business.PokemonData pokemonData, string pathFile)
        {
            using(FileStream file = new FileStream(Path.Combine(pathFile, "PokemonData.bin"), FileMode.Create))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, pokemonData);
                file.Close();
            }            
        }
    }
}
