﻿using CsvHelper;
using CsvHelper.Configuration;
using System.Globalization;


namespace PokemonData
{
    static internal class ReadCsv
    {
        static public void Read()
        {        
            using (var reader = new StreamReader("D:\\PokemonData.csv"))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Read();
                csv.ReadHeader();
                
                while (csv.Read())
                {
                    var record = csv.GetRecord<PokemonData.Business.PokemonData>();
                    Console.WriteLine($"{record.Name}, {record.Id}");                    
                }                
                
            }

        }
    }
}
