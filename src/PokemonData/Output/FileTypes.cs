﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonData
{
    static internal class FileTypes
    {
        public const string Xml = "xml";
        public const string Json = "json";
        public const string Binary = "binary";
        public const string Csv = "csv";
    }
}
