﻿using McMaster.Extensions.CommandLineUtils;
using System.Text.Json;
using PokemonData;
using PokemonData.Business;

class Program
{
        
    static async Task Main(string[] args)
    {
        
        var app = new CommandLineApplication();
        
        app.HelpOption();

        var pokemon = app.Argument("Pokemon's name", "Type exist Pokemon's name")
            .IsRequired();
                
        var fileType = app.Option("-f|--file <TYPE>", @"Please type format of file do you want to store: 
                                                        json, xml, csv, binary", CommandOptionType.MultipleValue);
        fileType.DefaultValue = FileTypes.Json;
        
        var path = app.Option("-p|--path <PathFile>", "Please type existing path to create file", CommandOptionType.SingleValue);
        path.DefaultValue = "D:\\";

        app.OnExecuteAsync(async (ctx) =>
        {
            string name = pokemon.Value;
            string pokemonName = name.ToLower();
            string pathFile = path.Value();

            PokemonDataManager pokemonInfo = new PokemonDataManager();            
            await pokemonInfo.LoadPokemonDataTo(pokemonName,fileType.Values, pathFile);            
            
        });

        try
        {
            await app.ExecuteAsync(args);
        } catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            app.ShowHelp();            
        }
                
    }   

}

