﻿
namespace PokemonData.Business
{
    [Serializable]
    public class PokemonData
    {        
        public string Name { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public int Height { get; set; }
        public bool Default { get; set; }
        public PokemonData(Data data)
        {
            this.Name = data.Name; 
            this.Id= data.Id;
            this.Type = data.Types[0].Type.Name;
            this.Height = data.Height;
            this.Default = data.IsDefault;
        }
        public PokemonData()
        {

        }

    }
}
