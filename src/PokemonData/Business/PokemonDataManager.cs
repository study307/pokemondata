﻿
namespace PokemonData.Business
{
    internal class PokemonDataManager
    {        
        private async Task<PokemonData> GetBusinessPokemonDataAsync(string pokemonName)
        {
            DataClient pokemonClient = new DataClient();
            var data = await pokemonClient.GetPokemonDataByNameAsync(pokemonName);
            PokemonData pokemonData = new PokemonData(data);
            return pokemonData;
        }
        private void Output(IReadOnlyList<string> fileType, PokemonData pokemonData, string pathFile)
        {
            if (!string.IsNullOrWhiteSpace(pathFile) && !Directory.Exists(pathFile))
            {                
                throw new Exception();
            }

            List<IOutput> output = OutputFactory.CreateOutputs(fileType);
            foreach (var outputFormat in output)
            {
                outputFormat.Output(pokemonData, pathFile);
            }
        }

        public async Task LoadPokemonDataTo(string pokemonName, IReadOnlyList<string> fileType, string pathFile)
        {
            var pokemonData = await GetBusinessPokemonDataAsync(pokemonName);
            Output(fileType, pokemonData, pathFile);
        }
        
    }
}
